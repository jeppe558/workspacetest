package opgave406;

import java.util.ArrayList;

public class TestApp {

	public static void main(String[] args) {
		TestClass blyat1 = new TestClass("OmegaBlyat", 25);
		TestClass blyat2 = new TestClass("MegaBlyat", 20);
		// comment

		ArrayList<TestClass> blyats = new ArrayList<>();

		blyats.add(blyat1);
		blyats.add(blyat2);

		for (TestClass blyat : blyats) {
			System.out.println("How big blyat: " + blyat.getBlyat() + " Number of cyka: " + blyat.getNoOfCyka());
		}

	}

}
